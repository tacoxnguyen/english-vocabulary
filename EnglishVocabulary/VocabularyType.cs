﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishVocabulary
{
    enum VocabularyType
    {
        None,
        Noun,
        Verb,
        Adverb,
        Adjective
    }
}
