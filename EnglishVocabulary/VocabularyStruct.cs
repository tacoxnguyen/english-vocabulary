﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EnglishVocabulary
{
    [DataContract]
    class VocabularyStruct
    {
        public VocabularyStruct()
        {
            Word = string.Empty;
            Type = VocabularyType.None;
            IPA = string.Empty;
            Definition = string.Empty;
            Example = string.Empty;
        }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Word { get; set; }
        [DataMember]
        public VocabularyType Type { get; set; }
        [DataMember]
        public string IPA { get; set; }
        [DataMember]
        public string Definition { get; set; }
        [DataMember]
        public string Example { get; set; }
    }
}
