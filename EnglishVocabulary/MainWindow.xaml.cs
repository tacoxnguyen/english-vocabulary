﻿using Hardcodet.Wpf.TaskbarNotification;
using Samples;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EnglishVocabulary
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Timer _firstTimeTimer;
        private VocabularyDb _db = new VocabularyDb();
        ObservableCollection<VocabularyStruct> _vocabularies;

        public MainWindow()
        {
            InitializeComponent();

            LoadDatabase();
            InitTimers();
            InitUI();
       }

        private void LoadDatabase()
        {
            _db.Load();
            _vocabularies = _db.GetAllVocabularies();
        }

        private void InitTimers()
        {
            var myTimer = new Timer();
            myTimer.Tick += new EventHandler(TimerEventProcessor);
            myTimer.Interval = 3 * 60 * 1000; // 3 minutes
            myTimer.Start();

            _firstTimeTimer = new Timer();
            _firstTimeTimer.Tick += new EventHandler(FirstTimerEventProcessor);
            _firstTimeTimer.Start();
        }

        private void InitUI()
        {
            gridVocabularies.ItemsSource = _db.GetAllVocabularies();

            var types = new List<VocabularyType> { VocabularyType.Noun, VocabularyType.Verb, VocabularyType.Adjective, VocabularyType.Adverb };
            Type.ItemsSource = types;
        }

        private void FirstTimerEventProcessor(Object myObject,
                                            EventArgs myEventArgs)
        {
            _firstTimeTimer.Stop();
            ShowNextVocabulary();
        }

        private void TimerEventProcessor(Object myObject,
                                            EventArgs myEventArgs)
        {
            ShowNextVocabulary();
        }

        private void ShowNextVocabulary()
        {
            FancyBalloon balloon = new FancyBalloon();

            var voc = _db.NextVocabulary();
            balloon.Vocabulary = voc.Word;
            balloon.Definition = voc.Definition;
            balloon.Example = voc.Example;
            balloon.IPA = voc.IPA;
            balloon.Type = voc.Type.ToString();

            MyNotifyIcon.ShowCustomBalloon(balloon, PopupAnimation.Slide, null);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ShowNextVocabulary();
        }

        private void EngVoc_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                var grid = (System.Windows.Controls.DataGrid)sender;
                if (grid.SelectedItems.Count > 0)
                {
                    var Res = System.Windows.MessageBox.Show("Are you sure you want to delete " + grid.SelectedItems.Count + " vocabularies?", "Deleting Records", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                    if (Res == MessageBoxResult.Yes)
                    {
                        var selectedItems = grid.SelectedItems;
                        for (int i = selectedItems.Count - 1; i >= 0; i--)
                        {
                            var item = selectedItems[i] as VocabularyStruct;
                            _db.RemoveVocabulary(item);   
                        }
                    }
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            _db.AddVocabulary(new VocabularyStruct());

            ScrollToEnd();
        }

        private void ScrollToEnd()
        {
            var border = VisualTreeHelper.GetChild(gridVocabularies, 0) as Decorator;
            if (border != null)
            {
                var scroll = border.Child as ScrollViewer;
                if (scroll != null) scroll.ScrollToEnd();
            }
        }
    }
}
