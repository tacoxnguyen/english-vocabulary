﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishVocabulary
{
    class VocabularyDb
    {
        SQLiteConnection _connection;

        private Random _rnd = new Random();
        private ObservableCollection<VocabularyStruct> _lstVocabularies = new ObservableCollection<VocabularyStruct>();
 
        private readonly string SQLITE_DATABASE_FILE = "vocabulary.sqlite";
        private readonly int SQLITE_VERSION = 3;

        private int _currentVoc = 0;

        public VocabularyDb()
        {
            _connection = new SQLiteConnection(string.Format("Data Source={0};Vertion={1};", SQLITE_DATABASE_FILE, SQLITE_VERSION));
            _connection.Open();

            var cmd = new SQLiteCommand(
                "create table if not exists vocabularies (" +
                "id	integer not null primary key autoincrement, " +
                "word varchar(128), ipa varchar(64), type integer, definition varchar(256), example varchar(256))", _connection);
            cmd.ExecuteNonQuery();
        }

        public void Load()
        {
            var cmd = new SQLiteCommand("select * from vocabularies", _connection);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
                _lstVocabularies.Add(new VocabularyStruct()
                {
                    Id = Int32.Parse(reader["id"].ToString()),
                    Word = reader["word"].ToString(),
                    IPA = reader["ipa"].ToString(),
                    Type = (VocabularyType) Int32.Parse(reader["type"].ToString()),
                    Definition = reader["definition"].ToString(),
                    Example = reader["example"].ToString()
                });
        }

        public bool AddWord(string word, string ipa, VocabularyType type, string defintion, string example)
        {
            var cmd = new SQLiteCommand(
                    "insert into [vocabularies] ([word], [ipa], [type], [definition], [example]) values ($word, $ipa, $type, $definition, $example)",
                    _connection);

            cmd.Parameters.AddWithValue("$word", word);
            cmd.Parameters.AddWithValue("$ipa", ipa);
            cmd.Parameters.AddWithValue("$type", type);
            cmd.Parameters.AddWithValue("$definition", defintion);
            cmd.Parameters.AddWithValue("$example", example);

            return (cmd.ExecuteNonQuery() == 1);
        }

        public bool DeleteWord(int id)
        {
            var cmd = new SQLiteCommand("delete from vocabularies where id=$id", _connection);
            cmd.Parameters.AddWithValue("$id", id);

            return (cmd.ExecuteNonQuery() == 1);
        }

        public VocabularyStruct NextVocabulary()
        {
            _currentVoc = _currentVoc % _lstVocabularies.Count;
            return _lstVocabularies[_currentVoc++];            
        }

        public ObservableCollection<VocabularyStruct> GetAllVocabularies()
        {
            return _lstVocabularies;
        }

        public void AddVocabulary(VocabularyStruct voc)
        {
            _lstVocabularies.Add(voc);
        }

        public void RemoveVocabulary(VocabularyStruct voc)
        {
            _lstVocabularies.Remove(voc);
        }

        public void Dispose()
        {
            _connection.Close();
        }
    }
}
